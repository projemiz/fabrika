#ifndef CALISAN_H
#define CALISAN_H

#include <QObject>
#include <QString>
#include <memory>
#include "temelverisinifi.h"
#include <veri_global.h>

using namespace std;

class VERI_EXPORT calisan : public temelverisinifi
{
    Q_OBJECT
public:
    typedef calisan veri;
    typedef shared_ptr<veri>ptr;
public:
    explicit calisan(QObject *parent = nullptr);

    IdTuru calisanid()const;
    void setCalisanid(const IdTuru &calisanid);

    Metin tckimlik() const;
    void setTckimlik(const Metin &tckimlik);

    Metin adi()const;
    void setAdi(const Metin &adi);

    Metin soyadi()const;
    void setSoyadi(const Metin &soyadi);

    Metin adres()const;
    void setAdres(const Metin &adres);

    Metin sicilno()const;
    void setSicilno(const Metin &sicilno);

    static ptr yeni() {return std::make_shared<veri>();}

    ptr kopyaOlustur(){
        auto kopya =yeni();
        kopya->_calisanid=this->_calisanid;
        kopya->_tckimlik=this->_tckimlik;
        kopya->_adi=this->_adi;
        kopya->_soyadi=this->_soyadi;
        kopya->_adres=this->_soyadi;
        kopya->_sicilno=this->_sicilno;
        return kopya;
    }


  signals:
    void calisanidDegisti(const IdTuru &calisanid);
    void tckimlikDegisti(const Metin &tckimlik);
    void adiDegisti(const Metin &adi);
    void soyadiDegisti(const Metin &soyadi);
    void adresDegisti(const Metin &adres);
    void sicilnoDegisti(const Metin &sicilno);



private:
    IdTuru _calisanid;
    Metin _tckimlik;
    Metin _adi;
    Metin _soyadi;
    Metin _adres;
    Metin _sicilno;










};

#endif // CALISAN_H
