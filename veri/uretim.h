#ifndef URETIM_H
#define URETIM_H

#include <QObject>
#include <memory>
#include "temelverisinifi.h"
#include <veri_global.h>

using namespace std;

class VERI_EXPORT uretim : public temelverisinifi
{
    Q_OBJECT
public:
    typedef  uretim veri;
    typedef shared_ptr<veri>ptr;

public:
    explicit uretim(QObject *parent = nullptr);

    IdTuru uretimid()const;
    void setUretimid(const IdTuru &uretimid);

    IdTuru vardiyaid() const;
    void setVardiyaid(const IdTuru &vardiyaid);

    IdTuru calisanid()const;
    void setCalisanid(const IdTuru &calisanid);

    IdTuru urunid()const;
    void setUrunid(const IdTuru &urunid);

    IsaretsizTamsayi miktar()const;
    void setMiktar(const IsaretsizTamsayi &miktar);

    inline IsaretsizTamsayi toplammiktar() {return _urunid *_miktar;}
    static ptr yeni() {return std::make_shared<veri>();}

    ptr kopyaOlustur() {
        auto kopya=yeni();
        kopya->_uretimid=this->_uretimid;
        kopya->_vardiyaid=this->_vardiyaid;
        kopya->_calisanid=this->_calisanid;
        kopya->_urunid=this->_urunid;
        kopya->_miktar=this->_miktar;
        return kopya;
    }

signals:
    void uretimidDegisti(const IdTuru &uretimid);
    void vardiyaidDegisti(const IdTuru &vardiyaid);
    void calisanidDegisti(const IdTuru &calisanid);
    void urunidDegisti(const IdTuru &urunid);
    void miktarDegisti(const IsaretsizTamsayi &miktar);
    void toplammiktarDegisti(const IsaretsizTamsayi &toplammiktar);






private:
    IdTuru _uretimid;
    IdTuru _vardiyaid;
    IdTuru _calisanid;
    IdTuru _urunid;
    IsaretsizTamsayi _miktar;



};

#endif // URETIM_H
