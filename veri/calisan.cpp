#include "calisan.h"

calisan::calisan(QObject *parent) : temelverisinifi(parent)
{
_calisanid =0;
_tckimlik ="";
_adi="";
_soyadi="";
_adres="";
_sicilno="";
}

calisan::IdTuru calisan::calisanid() const {
    return _calisanid;
}

void calisan::setCalisanid(const IdTuru &calisanid) {
    if(_calisanid != calisanid) {
        _calisanid =calisanid;
        calisanidDegisti(_calisanid);
    }
}

calisan::Metin calisan::tckimlik() const{
    return _tckimlik;
}

void calisan::setTckimlik(const Metin &tckimlik){
    if(_tckimlik != tckimlik) {
        _tckimlik =tckimlik;
        tckimlikDegisti(_tckimlik);
    }
}

calisan::Metin calisan::adi() const{
    return _adi;
}


void calisan::setAdi(const Metin &adi) {
    if(_adi !=adi) {
       _adi = adi;
       adiDegisti(_adi);
    }
}

calisan::Metin calisan::soyadi() const{
    return _soyadi;
}

void calisan::setSoyadi(const Metin &soyadi) {
    if(_soyadi != soyadi) {
        _soyadi =soyadi;
        soyadiDegisti(_soyadi);
    }
}

calisan::Metin calisan::adres() const {
    return _adres;
}


void calisan::setAdres(const Metin &adres) {
    if(_adres != adres) {
    _adres =adres;
    adresDegisti(_adres);
   }
}

calisan::Metin calisan::sicilno() const {
    return  _sicilno;
}
void calisan::setSicilno(const Metin &sicilno) {
    if(_sicilno != sicilno) {
        _sicilno =sicilno;
        sicilnoDegisti(_sicilno);
    }
}





















