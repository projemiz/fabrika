#ifndef VARDIYA_H
#define VARDIYA_H

#include <QObject>
#include <QDateTime>
#include <memory>
#include "temelverisinifi.h"
#include <veri_global.h>

using namespace std;


class VERI_EXPORT vardiya : public temelverisinifi
{
    Q_OBJECT
public:
    typedef vardiya veri;
    typedef shared_ptr<vardiya>ptr;

public:
    explicit vardiya(QObject *parent = nullptr);

    IdTuru vardiyaid()const;
    void setVardiyaid(const IdTuru &vardiyaid);

    TarihSaat vardiyabaslangıc()const;
    void setVardiyabaslangıc(const TarihSaat &vardiyabaslangıc);

    QDateTime vardiyabitis()const;
    void setVardiyabitis(const TarihSaat &vardiyabitis);
    static ptr yeni(){return std::make_shared<veri>();}

    ptr kopyaOlustur() {
        auto kopya=yeni();
        kopya->_vardiyaid=this->_vardiyaid;
        kopya->_vardiyabaslangıc=this->_vardiyabaslangıc;
        kopya->_vardiyabitis=this->_vardiyabitis;
        return kopya;
    }

signals:
    void vardiyaidDegisti(const IdTuru &vardiyaid);
    void vardiyabaslangıcDegisti(const TarihSaat &vardiyabaslangıc);
    void vardiyabitisDegisti(const TarihSaat &vardiyabitis);


private:
    IdTuru _vardiyaid;
    TarihSaat _vardiyabaslangıc;
    TarihSaat _vardiyabitis;







};

#endif // VARDIYA_H
